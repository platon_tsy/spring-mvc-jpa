package com.hillel.springmvc.dao;

import java.util.List;

import com.hillel.springmvc.model.User;


public interface UserRepository {

	User findById(int id);
	
	User findBySSO(String sso);
	
	void save(User user);
	
	void deleteBySSO(String sso);
	
	List<User> findAllUsers();

}

